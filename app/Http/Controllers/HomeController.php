<?php

namespace App\Http\Controllers;

use App\Models\backend\Employee;
use App\Models\backend\Task;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home() {

        $tasks = Task::latest()->paginate(5);


        return view('home',compact('tasks'));
    }


    public function details($id) {

        $task = Task::find($id);

        return view('single_project',compact('task'));
    }


    public function finished(Request $request) {
try{
        $currentStatus = $request->currentStatus;
        if($currentStatus == 'Pending'){
            $taskId = $request->taskId;

            $task = Task::find($taskId);
            $task->update([
                'status' => 'Finished',
            ]);
        }


        return response()->json(['status' => 'success', 'message' => 'Task Complete']);
    } catch (\Exception $e) {
        return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
    }
    }



    public function search(Request $request) {
        $tasks = Task::where('title','like', '%'.$request->search_string.'%')
            ->orWhere('status', 'like', '%'.$request->search_string.'%')
            ->orWhere('employee_name', 'like', '%'.$request->search_string.'%')
            ->orderBy('id', 'desc')
            ->paginate(5);

        if ($tasks->count() >= 1) {
            // Return a view for the table-data section
            return view('table-data', compact('tasks'));
        } else {
            return response()->json([
                'status' => 'nothing_found',
            ]);
        }
    }


}
