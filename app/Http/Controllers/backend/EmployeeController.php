<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\backend\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{

    public function index()
    {
        $employee = Employee::all();
        if (!$employee) {
            return response()->json(['error' => 'Employee not found '], 404);
        }
        if (request()->ajax()) {
            return response()->json($employee);
        }
        return view('admin.pages.all_employees', compact('employee'));
    }

    public function create()
    {

        return view('admin.pages.add_employee');
    }

    public function edit($id)
    {
        $employee = Employee::where('id', $id)->first();
        return view('admin.pages.edit_employee', compact('employee'));
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required|max:255',
                'email' => 'required|max:255',
                'mobile' => 'max:25',
                'designation' => 'max:55',
                'address' => 'max:255',
                'photo' => 'max:2000',
            ]);

            $employee = new Employee;
            $employee->name = $request->name;
            $employee->email = $request->email;
            $employee->mobile = $request->mobile;
            $employee->designation = $request->designation;
            $employee->address = $request->address;

            // Handle photo upload
            if ($request->hasFile('photo')) {
                $photo = $request->file('photo');
                $photoName = uniqid() . '.' . $photo->getClientOriginalExtension();
                $photo->move(public_path('backend/img/employee_photos'), $photoName);
                $employee->photo = $photoName;
            }

            $employee->save();

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }
    }



    public function update(Request $request)
    {
        try {
            $employee = Employee::find($request->employeeId);

            if (!$employee) {
                return response()->json(['error' => 'Employee not found '], 404);
            }

            // Check if the new email is different from the existing one
            if ($request->email !== $employee->email) {
                $request->validate([
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:employees,email,' . $request->employeeId,
                    'mobile' => 'max:25',
                    'designation' => 'max:55',
                    'address' => 'max:255',
                    'photo' => 'max:2000|image',
                ]);
            } else {
                $request->validate([
                    'name' => 'required|max:255',
                    'mobile' => 'max:25',
                    'designation' => 'max:55',
                    'address' => 'max:255',
                    'photo' => 'max:2000|image',
                ]);
            }
            // Update the Employee with the new data
            $employee->update([
                "name" => $request->name,
                "email" => $request->email,
                "mobile" => $request->mobile,
                "designation" => $request->designation,
                "address" => $request->address,
            ]);



            // Handle photo upload
            if ($request->hasFile('photo')) {
                $photo = $request->file('photo');
                $photoName = uniqid() . '.' . $photo->getClientOriginalExtension();
                $photo->move(public_path('backend/img/employee_photos'), $photoName);
                $employee->update(['photo' => $photoName]);
            }

            return response()->json(['status' => 'success', 'message' => 'Employee updated successfully']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }
    }


    public function destroy(Request $request)
    {
        try {
            $employeeID = $request->input('employeeId');
            $employee = Employee::find($employeeID);

            if (!$employee) {
                return response()->json(['status' => 'error', 'message' => 'employee not found'], 404);
            }

            $employee->delete();
            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }
    }


    public function search(Request $request)
    {
        try {
            $employee = Employee::where('name', 'like', '%' . $request->search_string . '%')
                ->orWhere('email', 'like', '%' . $request->search_string . '%')
                ->orWhere('mobile', 'like', '%' . $request->search_string . '%')
                ->orderBy('id', 'desc')
                ->paginate(5);

            if ($employee->count() >= 1) {
                // Return a view for the table-data section
                return view('admin.pages.table_data_employee', compact('employee'));
            } else {
                return response()->json([
                    'status' => 'nothing_found',
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }
    }
}
