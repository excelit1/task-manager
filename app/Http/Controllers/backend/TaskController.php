<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\backend\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::latest()->paginate(10);
        if (!$tasks) {
            return response()->json(['error' => 'Task not found '], 404);
        }
        return view('admin.pages.all_task', compact('tasks'));
    }

    public function create()
    {
        return view('admin.pages.add_task');
    }

    public function store(Request $request)
    {
        try {
            $taskData = $request->taskData;
            $totalTasks = count($taskData);

            for ($taskNo = 0; $taskNo < $totalTasks; $taskNo++) {
                $rules = [
                    'taskData.' . $taskNo . '.title' => 'required|max:255',
                    'taskData.' . $taskNo . '.duration' => 'required|numeric',
                    'taskData.' . $taskNo . '.employee_name' => 'required',
                    'taskData.' . $taskNo . '.description' => 'required',
                ];

                $messages = [
                    'taskData.' . $taskNo . '.title.required' => 'Title is required for Task ' . ($taskNo + 1),
                    'taskData.' . $taskNo . '.title.max' => 'Title for Task ' . ($taskNo + 1) . ' should not exceed :max characters',
                    'taskData.' . $taskNo . '.duration.required' => 'Duration is required for Task ' . ($taskNo + 1),
                    'taskData.' . $taskNo . '.duration.numeric' => 'Duration for Task ' . ($taskNo + 1) . ' should be a numeric value',
                    'taskData.' . $taskNo . '.employee_name.required' => 'Employee name is required for Task ' . ($taskNo + 1),
                    'taskData.' . $taskNo . '.description.required' => 'Description is required for Task ' . ($taskNo + 1),
                ];

                $validator = Validator::make($request->all(), $rules, $messages);

                if ($validator->fails()) {
                    return response()->json(['status' => 'error', 'errors' => $validator->errors()], 422);
                }

                // Create and save the task
                $task = new Task();
                $task->title = $taskData[$taskNo]['title'];
                $task->duration = $taskData[$taskNo]['duration'];
                $task->status = $taskData[$taskNo]['status'];
                $task->employee_name = $taskData[$taskNo]['employee_name'];
                $task->description = $taskData[$taskNo]['description'];

                $task->save();
            }

            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(
                ['status' => 'error', 'message' => $e->getMessage() . $e->getLine()],
                500
            );
        }
    }

    public function edit($id)
    {
        $tasks = Task::all();
        if (!$tasks) {
            return response()->json(['error' => 'Task not found '], 404);
        }
        return view('admin.pages.edit_task', compact('tasks'));
    }

    public function update(Request $request)
    {
        try {
            $request->validate([
                'id' => 'required|numeric',
                'title' => 'required',
                'duration' => 'required',
                'employee_name' => 'required',
                'description' => 'required',
            ]);
            $task = Task::find($request->id);

            if (!$task) {
                return response()->json(['error' => 'Task not found'], 404);
            }

            // Update the Task with the new data
            $task->update([
                'title' => $request->title,
                'duration' => $request->duration,
                'employee_name' => $request->employee_name,
                'description' => $request->description,
            ]);

            return response()->json([
                'status' => 'success',
                'message' => 'Task updated successfully',
            ]);
        } catch (\Exception $e) {
            return response()->json(
                ['status' => 'error', 'message' => $e->getMessage()],
                500
            );
        }
    }

    public function destroy(Request $request)
    {
        try {
            $taskId = $request->input('taskId');
            $task = Task::find($taskId);

            if (!$task) {
                return response()->json(
                    ['status' => 'error', 'message' => 'Task not found'],
                    404
                );
            }

            $task->delete();
            return response()->json(['status' => 'success']);
        } catch (\Exception $e) {
            return response()->json(
                ['status' => 'error', 'message' => $e->getMessage()],
                500
            );
        }
    }

    public function search(Request $request)
    {
        try {
            $tasks = Task::where(
                'title',
                'like',
                '%' . $request->search_string . '%'
            )
                ->orWhere('status', 'like', '%' . $request->search_string . '%')
                ->orWhere(
                    'employee_name',
                    'like',
                    '%' . $request->search_string . '%'
                )
                ->orderBy('id', 'desc')
                ->paginate(10);

            if ($tasks->count() >= 1) {
                // Return a view for the table-data section
                return view('admin.pages.table-data', compact('tasks'));
            } else {
                return response()->json([
                    'status' => 'nothing_found',
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(
                ['status' => 'error', 'message' => $e->getMessage()],
                500
            );
        }
    }
    public function assain(Request $request)
    {
        try {
            $allTaskId = $request->taskId;
            $employee = $request->taskFor;

            foreach ($allTaskId as $id) {
                $task = Task::find($id);

                if (!$task) {
                    return response()->json(['error' => 'Task not found'], 404);
                }

                $task->update([
                    'employee_name' => $employee
                ]);
            }

            $tasks = Task::latest()->paginate(10);

            return view('admin.pages.table-data', compact('tasks'));


            // return response()->json([
            //     'status' => 'success',
            //     'message' => 'Task updated successfully',
            // ]);
        } catch (\Exception $e) {
            return response()->json(
                ['status' => 'error', 'message' => $e->getMessage()],
                500
            );
        }
    }
}
