@extends('admin.layouts.master')

@section('content')
    <!-- Single pro tab start-->
    <div class="single-product-tab-area mg-b-30">
        <!-- Single pro tab review Start-->
        <div class="single-pro-review-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="review-tab-pro-inner">
                            <ul id="myTab3" class="tab-review-design">
                                <li class="active"><a href="#description"><i class="icon nalika-edit" aria-hidden="true"></i>
                                        Task ADD</a>
                                </li>
                            </ul>

                            <div id="myTabContent" class="tab-content custom-product-edit">
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="container">
                                        <div id="allForm">

                                            <form id="taskForm" method="POST" action="{{ route('task.store') }}">
                                                @csrf
                                                <div id="singleTask">
                                                    <div class="row">
                                                        <h3 class="  text-danger">Task 1</h3>
                                                        <br>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="review-content-section">
                                                                <div class="input-group mg-b-pro-edt">
                                                                    <span class="input-group-addon"><i
                                                                            class="icon nalika-edit"
                                                                            aria-hidden="true"></i></span>
                                                                    <select
                                                                        class="form-control employee_name default_employee"
                                                                        name="employee_name" id="employee_name"
                                                                        placeholder="">
                                                                        <option>Employee Select - None</option>
                                                                        @php($employees = App\Models\backend\Employee::all())
                                                                        @foreach ($employees as $employee)
                                                                            <option>{{ $employee->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>

                                                                <div class="input-group mg-b-pro-edt">
                                                                    <span class="input-group-addon"><i
                                                                            class="icon nalika-edit"
                                                                            aria-hidden="true"></i></span>

                                                                    <input type="text" id="title" name="title"
                                                                        class="form-control " placeholder="Task Title">
                                                                </div>

                                                                <div class="input-group mg-b-pro-edt">
                                                                    <span class="input-group-addon"><i
                                                                            class="icon nalika-edit"
                                                                            aria-hidden="true"></i></span>

                                                                    <input type="number" id="duration" name="duration"
                                                                        class="form-control"
                                                                        placeholder="Task Duration in hours">
                                                                </div>

                                                                <div class="input-group mg-b-pro-edt">
                                                                    <span class="input-group-addon"><i
                                                                            class="icon nalika-new-file"
                                                                            aria-hidden="true"></i></span>

                                                                    <textarea class="form-control" id="descriptionText" name="descriptionText" rows="10" cols="150"
                                                                        style="background: #1B2A47;color:rgb(255, 255, 255)" placeholder="Task Description.."></textarea>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div id="errMessage" class=""></div>
                                                        <div class="text-center custom-pro-edt-ds">
                                                            <button id="save" type="submit"
                                                                class="btn btn-ctl-bt waves-effect waves-light m-r-10">Save</button>
                                                            <button id="addMulti" type="button"
                                                                class="btn btn-ctl-bt waves-effect waves-light m-r-10">Add
                                                                Another</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $(document).ready(function() {


                    // $('.default_employee').on('change', function() {
                    //     let defaultEmployeeValue = $('.default_employee').val();
                    //     allselectEmployee.push(defaultEmployeeValue);
                    // });

                    //add multiform
                    let clickCoutn = 0;
                    $(document).on("click", '#addMulti', function(e) {
                        e.preventDefault();
                        let clickNo = clickCoutn++;

                        let singleTask = `
                        <div id="taskRow${clickNo}" class="row">
                            <br>
                            <h3 class=" text-danger">Task ${clickNo+2}</h3>
                            <br>
                            <div id="errMessage${clickNo}" class=""></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="review-content-section">
                                    <div class="input-group mg-b-pro-edt">
                                        <span class="input-group-addon"><i class="icon nalika-edit"
                                                aria-hidden="true"></i></span>
                                        <select class="form-control employee_name" name="employee_name"
                                            id="employee_name${clickNo}" placeholder="">
                                            <option>Employee Select - None</option>
                                            @php($employees = App\Models\backend\Employee::all())
                                            @foreach ($employees as $employee)
                                                <option>{{ $employee->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="input-group mg-b-pro-edt">
                                        <span class="input-group-addon"><i class="icon nalika-edit"
                                                aria-hidden="true"></i></span>
                                        <input type="text" name="title" id="title${clickNo}" class="form-control"
                                            placeholder="Task Title">
                                    </div>

                                    <div class="input-group mg-b-pro-edt">
                                        <span class="input-group-addon"><i class="icon nalika-edit"
                                                aria-hidden="true"></i></span>
                                        <input type="number" id="duration${clickNo}" name="duration" class="form-control"
                                            placeholder="Task Duration in hours">
                                    </div>

                                    <div class="input-group mg-b-pro-edt">
                                        <span class="input-group-addon"><i class="icon nalika-new-file"
                                                aria-hidden="true"></i></span>
                                        <textarea class="form-control" id="descriptionText${clickNo}" name="descriptionText" rows="10" cols="150"
                                            style="background: #1B2A47;color:rgb(255, 255, 255)"
                                            placeholder="Task Description.."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>`;

                        $('#singleTask').append(singleTask);

                        // allselectEmployee.forEach(oldName => {
                        //     if (this.value == oldName) {
                        //         allselectEmployee.pop(this.value);
                        //         $(`#taskRow${clickNo}`).remove();
                        //         Command: toastr["info"]("Task remove. Already have this employee");

                        //         return false;
                        //     }
                        // });
                        //chack double employee
                    });

                    //chack double employee
                    let allSelectEmployee = [];
                    $(document).on('change', '.employee_name', function() {
                        let selectedValue = $(this).val();
                        let taskRow = $(this).closest('.row');

                        if (allSelectEmployee.includes(selectedValue)) {
                            taskRow.remove();
                            allSelectEmployee.pop(selectedValue);
                            toastr["info"]("Task removed. Employee already selected.");
                        } else {
                            let prevSelectedValue = taskRow.data('selected-employee');
                            if (prevSelectedValue) {
                                let index = allSelectEmployee.indexOf(prevSelectedValue);
                                if (index !== -1) {
                                    allSelectEmployee.splice(index, 1);
                                }
                            }
                            taskRow.data('selected-employee', selectedValue);
                            allSelectEmployee.push(selectedValue);
                        }
                        console.log(allSelectEmployee);
                    });




                    //store
                    $(document).on("click", '#save', function(e) {
                        e.preventDefault();

                        let taskData = [];
                        let employeeName = $('#employee_name').val();
                        if (employeeName == "Employee Select - None") {
                            employeeName = "-"
                        }

                        let initTask = {
                            title: $('#title').val(),
                            status: 'Pending',
                            duration: $('#duration').val(),
                            employee_name: employeeName,
                            description: $('#descriptionText').val()
                        };
                        taskData.push(initTask);

                        if (clickCoutn >= 0) {
                            for (let i = 0; i < clickCoutn; i++) {
                                let employeeName = $(`#employee_name${i}`).val();
                                if (employeeName == "Employee Select - None") {
                                    employeeName = "-"
                                }
                                let task = {
                                    title: $(`#title${i}`).val(),
                                    status: 'Pending',
                                    duration: $(`#duration${i}`).val(),
                                    employee_name: employeeName,
                                    description: $(`#descriptionText${i}`).val()
                                };
                                taskData.push(task);
                            }
                        }

                        $.ajax({
                            url: "{{ route('task.store') }}",
                            method: "post",
                            data: {
                                taskData
                            },
                            success: function(response) {
                                if (response.status == 'success') {
                                    console.log(response.data);
                                    $('#taskForm')[0].reset();

                                    Command: toastr["success"]("Task added successfully");
                                }
                            },
                            error: function(err) {
                                console.log(err);
                                let error = err.responseJSON;

                                if (error.status === 'error' && error.errors) {
                                    toastr["warning"]("Validation error. Please check the form.");

                                    $(`#errMessage`).empty();
                                    $.each(error.errors, function(index, value) {
                                        $(`#errMessage`).append('<ul class="text-danger"><li>' +
                                            value + '</li></ul><br>');
                                    });
                                } else {
                                    toastr["error"](
                                        "Sorry, something went wrong. Please try again later.");
                                }
                            }
                        });
                    });
                });
            </script>
        @endsection
