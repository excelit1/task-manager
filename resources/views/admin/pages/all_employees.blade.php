@extends('admin.layouts.master')
@section('content')
    <div class="product-status mg-b-30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="product-status-wrap">
                        <h4>Employees List</h4>
                        <div class="add-product">
                            <a href="product-edit.html">Add Employee</a>
                        </div>

                        <div class="from-control">
                            <input class="" type="search"
                            style="margin-bottom: 20px; padding:8px; width:350px" 
                            name="search" id="search"
                            placeholder="Search by name or email or mobile">
                        </div>
                        {{-- <table>
                        <tr>
                            <th>Task Title</th>
                            <th>Status</th>
                            <th>Description</th>
                            <th>Setting</th>
                        </tr>
                        <tr id="taskData">
                            <td id="taskTitle"></td>
                            <td>
                                <button class="pd-setting">Active</button>
                            </td>
                           <td >Lorem ipsum dolor sit, amet consectetur adipisicing elit. ?lorem</td>
                           
                           
                            <td>
                                <button data-toggle="tooltip" title="Edit" class="pd-setting-ed"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button data-toggle="tooltip" title="Trash" class="pd-setting-ed"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>Task Title 2</td>
                            <td>
                                <button class="ps-setting">Paused</button>
                            </td>
                           <td >Lorem ipsum dolor sit, amet consectetur adipisicing elit. ?</td>
                            
                            <td>
                                <button data-toggle="tooltip" title="Edit" class="pd-setting-ed"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <button data-toggle="tooltip" title="Trash" class="pd-setting-ed"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                            </td>
                        </tr>
                    </table> --}}
                        <table >
                            <tr>
                                <th>Employee photo</th>
                                <th>Employee name</th>
                                <th>Employee email</th>
                                <th>Employee mobile</th>
                                <th>Employee designation</th>
                                <th>Employee address</th>

                            </tr>
                            <tbody class="table-data" id="employeeData">
                            </tbody>
                        </table>

                        <div class="custom-pagination">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $.ajax({
                type: 'GET',
                url: '/employees',
                success: function(data) {
                    data.forEach(function(employee) {
                        var statusClass = employee.status === 'Active' ? 'pd-setting' :
                            'ps-setting';
                        var statusText = employee.status === 'Active' ? 'Active' : 'Paused';

                        var imageUrl = employee.photo ?
                            `{{ asset('backend/img/employee_photos/') }}/${employee.photo}` :
                            `{{ asset('backend/img/product/1.jpg') }}`;
                        console.log(employee)
                        var row = `
                        <tr>
                            <td>
                                <img style="width: 120px; height: 100px" src="${imageUrl}" alt="">
                            </td>
                            <td>${employee.name}</td>
                            <td>${employee.email}</td>
                            <td>${employee.mobile}</td>
                            <td>${employee.designation}</td>
                            <td>${employee.address}</td>
                            <td>
                                <a href="/employee/edit/${employee.id}">
                                    <button data-toggle="tooltip" title="Edit" class="pd-setting-ed">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </button>
                                    </a>
                                    
                                    <button data-toggle="tooltip" data-employee-id="${ employee.id }" title="Trash" class="pd-setting-ed deleteEmployee">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                               
                            </td>
                        </tr>
                    `;

                        $('#employeeData').append(row);
                    });
                },
                error: function(error) {
                    console.log('Error:', error);
                }
            });


            //delete employee
            $(document).on("click", '.deleteEmployee', function(e) {
                e.preventDefault();
                let clickedButton = $(this);
                let employeeId = clickedButton.data('employee-id');

                if (confirm("Are you sure you want to delete this employee?")) {
                    $.ajax({
                        url: "{{ route('employee.delete') }}",
                        method: "post",
                        data: {
                            employeeId: employeeId
                        },
                        success: function(response) {
                            if (response.status == 'success') {
                                clickedButton.closest("tr").remove();
                                Command: toastr["success"]("Employee Delete successfully")
                                
                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "newestOnTop": true,
                                    "progressBar": true,
                                    "positionClass": "toast-top-right",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                }
                            }
                        },
                        error: function(err) {
                            console.log(err);
                            let error = err.responseJSON;
                            $('#errMessage').empty();
                            $.each(error.errors, function(index, value) {
                                $('#errMessage').append('<ul class="text-danger"><li>' +
                                    value + '</li></ul><br>');
                            });
                        }
                    });
                }
            });




            // search task 
            $(document).on("keyup", function(e) {
                e.preventDefault();

                let search_string = $('#search').val();
                console.log(search_string);
                $.ajax({
                    url: "{{ route('employee.search') }}",
                    method: "GET",
                    data: {
                        search_string: search_string
                    },
                    success: function(response) {
                        console.log(response);
                        $('.table-data').html(response);
                        if (response.status == 'nothing_found') {
                            $('.table-data').html('<span class="text-danger">' +
                                'Nothing Found' + '</span>');
                        }
                    },
                });
            });
        });
    </script>
@endsection
