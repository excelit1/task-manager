@extends('admin.layouts.master')
@section('content')
    <div class="product-status mg-b-30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="product-status-wrap">
                        <h4>Tasks List</h4>
                        @if (session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif

                        <div id="errMessage"></div>
                        <div class="d-flex" style="display:flex; width: 100% ">


                            <div class="from-control" style="width:350px">
                                <input class="" type="search" style="margin-bottom: 20px; padding:8px; width:350px"
                                    name="search" id="search" placeholder="Search by title or name or status">
                            </div>
                            <div class="">

                            </div>
                            <p id="task-assain display-none" style="color:white; margin-left: auto; padding-top: 10px ">Task
                                For: </p>
                            <div class="task-assain ms-auto" style="width:350px; margin-left: 30px; ">
                                <select class="form-control" id="employee_name" name="employee_name"
                                    id="exampleFormControlSelect1" placeholder="">
                                    <option>{{ $tasks[0]->employee_name }}</option>
                                    @php($employees = App\Models\backend\Employee::all())
                                    @foreach ($employees as $employee)
                                        <option id="taskAssainEmployee">{{ $employee->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="add-product">
                            <a href="{{ route('task.create') }}">Add Task</a>
                        </div>

                        <table id="taskData">
                            <thead>

                                <tr>
                                    <th>CK</th>
                                    <th>ID</th>
                                    <th>Task Title</th>
                                    <th>Employee name</th>
                                    <th>Status</th>
                                    <th>Setting</th>
                                </tr>
                            </thead>
                            {{-- <tr></tr> --}}
                            <tbody class="table-data">

                                @foreach ($tasks as $key => $task)
                                    <tr>
                                        <input type="hidden" id="taskId" value="{{ $task->id }}" name="">
                                        <td>
                                            <div class="form-check">
                                                <input class="form-check-input p-5 taskCheck" type="checkbox" value=""
                                                    id="{{ $task->id }}">
                                            </div>
                                        </td>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $task->title }}</td>
                                        <td>{{ $task->employee_name }}</td>
                                        <td><button
                                                class="{{ $task->status === 'Pending' ? 'ps-setting Active' : 'pd-setting Active' }}">{{ $task->status }}</button>
                                        </td>
                                        <td>
                                            <a href="/task/edit/{{ $task->id }}">
                                                <button data-toggle="tooltip" title="Edit" class="pd-setting-ed">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </button>
                                            </a>
                                            <button data-toggle="tooltip" data-task-id="{{ $task->id }}" title="Trash"
                                                class="pd-setting-ed deleteTask">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </button>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $tasks->links() }}

                        <div class="custom-pagination">
                            {{-- <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul> --}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $(document).on("click", '.deleteTask', function(e) {
                e.preventDefault();
                let clickedButton = $(this);
                let taskId = clickedButton.data('task-id');

                if (confirm("Are you sure you want to delete this task?")) {
                    $.ajax({
                        url: "{{ route('task.delete') }}",
                        method: "post",
                        data: {
                            taskId: taskId
                        },
                        success: function(response) {
                            if (response.status == 'success') {
                                clickedButton.closest("tr").remove();
                                Command: toastr["success"]("Task Delete successfully")

                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "newestOnTop": true,
                                    "progressBar": true,
                                    "positionClass": "toast-top-right",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                }
                            }
                        },
                        error: function(err) {
                            console.log(err);
                            let error = err.responseJSON;
                            $('#errMessage').empty();
                            $.each(error.errors, function(index, value) {
                                $('#errMessage').append('<ul class="text-danger"><li>' +
                                    value + '</li></ul><br>');
                            });
                        }
                    });
                }
            });


            // search task
            $(document).on("keyup", function(e) {
                e.preventDefault();

                let search_string = $('#search').val();
                // console.log(search_string);
                $.ajax({
                    url: "{{ route('task.search') }}",
                    method: "GET",
                    data: {
                        search_string: search_string
                    },
                    success: function(response) {
                        // console.log(response);
                        $('.table-data').html(response);
                        if (response.status == 'nothing_found') {
                            $('.table-data').html('<span class="text-danger">' +
                                'Nothing Found' + '</span>');
                        }
                    },
                });
            });


            // multi assain check
            let storeTaskChecked = [];
            // let taskFor = do
            $(document).on("change", '.taskCheck', function(e) {
                // e.preventDefault();
                if (this.checked) {
                    let empty = [];
                    storeTaskChecked = empty;
                    $(".taskCheck:checked").each(function() {
                        let taskId = ($(this).attr('id'));
                        storeTaskChecked.push(taskId);
                    });
                    console.log(`You click me, My value is: `);
                } else {

                    let taskId = ($(this).attr('id'));
                    storeTaskChecked.pop(taskId);
                    console.log('YOu unchecked me');
                }
            });


            //task assain

            $('#employee_name').change(function() {
                var selectedValue = $(this).val();

                console.log(storeTaskChecked);
                console.log(selectedValue);
                $.ajax({
                    url: "{{ route('task.assain') }}",
                    method: "POST",
                    data: {
                        taskId: storeTaskChecked,
                        taskFor: selectedValue
                    },
                    success: function(response) {
                        console.log(response);
                        $('.table-data').html(response);
                        Command: toastr["success"]("Employee Change successfully")

                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": true,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        }

                    },
                });
            });






        });
    </script>
@endsection
