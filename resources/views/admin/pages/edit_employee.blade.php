@extends('admin.layouts.master')

@section('content')
    <!-- Single pro tab start-->
    <div class="single-product-tab-area mg-b-30">
        <!-- Single pro tab review Start-->
        <div class="single-pro-review-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="review-tab-pro-inner">
                            <ul id="myTab3" class="tab-review-design">
                                <li class="active"><a href="#description"><i class="icon nalika-edit" aria-hidden="true"></i>
                                        Employee Edit</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <form action="" method="POST" id="employeeForm" enctype="multipart/form-data">


                                        <div class="container">

                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="review-content-section">


                                                        <div id="errMessage"></div>
                                                    <input type="hidden" id="employeeId" value="{{$employee->id}}" name="">
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-edit"
                                                                    aria-hidden="true"></i></span>
                                                            <input type="text" id="name"
                                                                value="{{ $employee->name }}" class="form-control"
                                                                placeholder="Employee Name">
                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-edit"
                                                                    aria-hidden="true"></i></span>
                                                            <input id="email" type="email"
                                                                value="{{ $employee->email }}" class="form-control"
                                                                placeholder="Employee email">
                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-edit"
                                                                    aria-hidden="true"></i></span>
                                                            <input id="mobile" type="text"
                                                                value="{{ $employee->mobile }}" class="form-control"
                                                                placeholder="Employee mobile">
                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-edit"
                                                                    aria-hidden="true"></i></span>
                                                            <input id="designation" type="text"
                                                                value="{{ $employee->designation }}" class="form-control"
                                                                placeholder="Designation">
                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-edit"
                                                                    aria-hidden="true"></i></span>
                                                            <input id="address" type="text"
                                                                value="{{ $employee->address }}" class="form-control"
                                                                placeholder="address">
                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-edit"
                                                                    aria-hidden="true"></i></span>
                                                            <input type="file" id="photo" class="form-control"
                                                                accept="image/*">
                                                            <div>
                                                                <img id="imgPreview" style="width: 170px; height: 150px"
                                                                    src="{{ !empty($employee->photo) ? asset('backend/img/employee_photos/' . $employee->photo) : asset('backend/img/product/1.jpg') }}"
                                                                    alt="">
                                                            </div>
                                                        </div>




                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="text-center custom-pro-edt-ds">
                                                    <button id="save" type="button"
                                                        class="btn btn-ctl-bt waves-effect waves-light m-r-10">Save
                                                    </button>
                                                    <button type="button"
                                                        class="btn btn-ctl-bt waves-effect waves-light">Discard
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(() => {
                // Store employee
                $(document).on("click", '#save', function(e) {
                    e.preventDefault();
                    let employeeId = $('#employeeId').val();
                    let name = $('#name').val();
                    let email = $('#email').val();
                    let mobile = $('#mobile').val();
                    let designation = $('#designation').val();
                    let address = $('#address').val();
                    let photo = $('#photo')[0].files[0]; 

                    console.log(photo);


                    // Create a FormData object to send file data
                    let formData = new FormData();
                    formData.append('employeeId', employeeId);
                    formData.append('name', name);
                    formData.append('email', email);
                    formData.append('mobile', mobile);
                    formData.append('designation', designation);
                    formData.append('address', address);
                    if(photo){

                        formData.append('photo', photo);
                    }

                    $.ajax({
                        url: "{{ route('employee.update') }}",
                        method: "Post",
                        data: formData,
                        contentType: false,
                        processData: false, 
                        success: function(response) {
                            if (response.status == 'success') {
                                $('#employeeForm')[0].reset();
                                Command: toastr["success"]("Employee update successfully")
                                
                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "newestOnTop": true,
                                    "progressBar": true,
                                    "positionClass": "toast-top-right",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                }
                            }
                        },
                        error: function(err) {
                            console.log(err.responseJSON);
                            let error = err.responseJSON;
                            $('#errMessage').empty();
                            $.each(error.errors, function(index, value) {
                                $('#errMessage').append('<ul class="text-danger"><li>' +
                                    value + '</li></ul><br>');
                            });
                        }
                    });
                });

                // Change photo preview when selecting a photo
                $('#photo').change(function() {
                    const file = this.files[0];
                    if (file) {
                        let reader = new FileReader();
                        reader.onload = function(event) {
                            $('#imgPreview').attr('src', event.target.result);
                        }
                        reader.readAsDataURL(file);
                    }
                });
            });
        </script>
    @endsection
