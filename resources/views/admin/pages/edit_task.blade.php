@extends('admin.layouts.master')

@section('content')
    <!-- Single pro tab start-->
    <div class="single-product-tab-area mg-b-30">
        <!-- Single pro tab review Start-->
        <div class="single-pro-review-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="review-tab-pro-inner">
                            <ul id="myTab3" class="tab-review-design">
                                <li class="active"><a href="#description"><i class="icon nalika-edit" aria-hidden="true"></i>
                                        Task Edit</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="container">
                                        <form id="taskForm" method="POST">
                                            {{ method_field('PUT') }}
                                            @csrf

                                            <div class="row">

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    @if ($errors->any())
                                                        <div class="alert alert-danger">
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif
                                                    @if (session()->has('message'))
                                                        <div class="alert alert-success">
                                                            {{ session()->get('message') }}
                                                        </div>
                                                    @endif
                                                    <div id="successMessage"></div>


                                                    <div class="review-content-section">
                                                        <input type="hidden" id="updateId" value="{{ $tasks[0]->id }}"
                                                            name="id">
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-edit"
                                                                    aria-hidden="true"></i></span>

                                                            <input type="text" id="title" name="title"
                                                                value="{{ $tasks[0]->title }}" class="form-control"
                                                                placeholder="Task Title">

                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-edit"
                                                                    aria-hidden="true"></i></span>

                                                            <input type="number" id="duration"
                                                                value="{{ $tasks[0]->duration }}" name="duration"
                                                                class="form-control" placeholder="Task Duration in houre">

                                                        </div>
                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-edit"
                                                                    aria-hidden="true"></i></span>
                                                            <select class="form-control" id="employee_name"
                                                                name="employee_name" id="exampleFormControlSelect1"
                                                                placeholder="">
                                                                <option>{{ $tasks[0]->employee_name }}</option>
                                                                @php($employees = App\Models\backend\Employee::all())
                                                                @foreach ($employees as $employee)
                                                                    <option>{{ $employee->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div class="input-group mg-b-pro-edt">
                                                            <span class="input-group-addon"><i class="icon nalika-new-file"
                                                                    aria-hidden="true"></i></span>

                                                            <textarea class="from-control" id="descriptionText" name="description" rows="20" cols="150"
                                                                style="background: #1B2A47;color:rgb(255, 255, 255)" placeholder="Task Description..">{{ $tasks[0]->description }}"</textarea>
                                                        </div>

                                                    </div>


                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="text-center custom-pro-edt-ds">
                                                        <button type="submit" id="save"
                                                            class="btn btn-ctl-bt waves-effect waves-light m-r-10">Save</button>
                                                        <button type="button"
                                                            class="btn btn-ctl-bt waves-effect waves-light">Discard</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {

                $(document).on("click", '#save', function(e) {
                    e.preventDefault();
                    let id = $('#updateId').val();
                    let title = $('#title').val();
                    let status = 'Pending';
                    let duration = $('#duration').val();
                    let employee_name = $('#employee_name').val();
                    let description = $('#descriptionText').val();
                    $.ajax({
                        url: " {{ route('task.update') }}",
                        method: "put",
                        data: {
                            id: id,
                            title: title,
                            duration: duration,
                            status: status,
                            employee_name: employee_name,
                            description: description
                        },
                        success: function(response) {
                            if (response.status == 'success') {
                                Command: toastr["success"]("task update successfully")
                                
                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "newestOnTop": true,
                                    "progressBar": true,
                                    "positionClass": "toast-top-right",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                }
                                // location.href = "{{ route('task.index') }}";

                            }
                        },
                        error: function(err) {
                            console.log(err);
                            let error = err.responseJSON;
                            $('#errMessage').empty();
                            $.each(error.errors, function(index, value) {
                                $('#errMessage').append('<ul class="text-danger"><li>' +
                                    value + '</li></ul><br>');
                            })
                        }
                    });
                });
            });
        </script>
    @endsection
