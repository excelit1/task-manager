<div class="table-data">

    @foreach ($tasks as $key => $task)
        <tr>
            <input type="hidden" id="taskId" value="{{ $task->id }}" name="">
            <td>
                <div class="form-check">
                    <input class="form-check-input p-5 taskCheck" type="checkbox" value="" id="{{ $task->id }}">
                </div>
            </td>
            <td>{{ $key + 1 }}</td>
            <td>{{ $task->title }}</td>
            <td>{{ $task->employee_name }}</td>
            <td><button
                    class="{{ $task->status === 'Pending' ? 'ps-setting Active' : 'pd-setting Active' }}">{{ $task->status }}</button>
            </td>
            <td>
                <a href="/task/edit/{{ $task->id }}">
                    <button data-toggle="tooltip" title="Edit" class="pd-setting-ed">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </button>
                </a>
                <button data-toggle="tooltip" data-task-id="{{ $task->id }}" title="Trash"
                    class="pd-setting-ed deleteTask">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </button>

            </td>
        </tr>
    @endforeach
</div>
