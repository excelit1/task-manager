<div class="table-data">

    @foreach ($employee as $key => $employee)
    <tr>
        <td>
            <img style="width: 120px; height: 100px" src="{{empty($employee->photo) ? asset('backend/img/product/1.jpg') : asset('backend/img/employee_photos/'.$employee->photo) }}" alt="">
        </td>
        <td>{{$employee->name}}</td>
        <td>{{$employee->email}}</td>
        <td>{{$employee->mobile}}</td>
        <td>{{$employee->designation}}</td>
        <td>{{$employee->address}}</td>
        <td>
            <a href="/employee/edit/'{{$employee->id}}">
                <button data-toggle="tooltip" title="Edit" class="pd-setting-ed">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </button>
                </a>
                
                <button data-toggle="tooltip" data-employee-id="{{$employee->id}}" title="Trash" class="pd-setting-ed deleteEmployee">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
            </button>
           
        </td>
    </tr>
    @endforeach
    </div>