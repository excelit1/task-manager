<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

    <!-- ajax JS
  ============================================ -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"
        integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <!-- toster
  ============================================ -->
  <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

</head>

<body>
    <a href="{{ route('dashboard') }}">
        <button type="submit" class="btn btn-outline-success float-end m-5">Admin</button>
    </a>


    <section class="vh-100" style="background-color: #eee;">
        <div class="container-fluid h-100">
            <div class="row d-flex justify-content-center align-items-center h-75">
                <div class="col col-lg-12 col-xl-9">
                    <div class="card rounded-3">
                        <div class="card-body p-4">

                            <h4 class="text-center my-3 pb-3">Task Manager</h4>

                            <form class="row  g-3 justify-content-center align-items-center mb-4 pb-2">
                                <div class="col-6 d-flex justify-content-center align-items-center">
                                    <div class="form-outline col-12">
                                        <input type="text" id="search" class="form-control "
                                            placeholder="Search by title or name or status..." />
                                    </div>
                                </div>


                                <div class="col-3">
                                    <button type="submit" id="" class="btn btn-warning">Search tasks</button>
                                </div>
                            </form>

                            <table class="table mb-4">
                                <thead>
                                    <tr>
                                        <th scope="col">No.</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Hour</th>
                                        <th scope="col">For</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Action</th>
                                        {{-- <th scope="col">Actions</th> --}}
                                    </tr>
                                </thead>
                                <tbody class="table-data">
                                    @foreach ($tasks as $key => $task)
                                        <tr >
                                            <th scope="row">{{ $key + 1 }}</th>
                                            <td>
                                                <a class="text-decoration-none" href="{{ '/project/' . $task->id }}">
                                                    {{ $task->title }}
                                                </a>
                                            </td>
                                            <td>{{ $task->duration }}</td>
                                            <td>{{ $task->employee_name }}</td>
                                            <td>
                                                <button id="status{{$task->id}}" class="{{$task->status == 'Pending' ? 'btn btn-secondary' : 'btn btn-success'}}">

                                                    {{ $task->status }}
                                                </button>
                                            </td>
                                            <td>
                                                <button data-task-id="{{$task->id}}" id="done" class="btn btn-primary ">Done</button>
                                            </td>
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>

                            {{$tasks->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>















 <!-- toster JS
    ============================================ -->
    <script src="http://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
        <script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
        {!! Toastr::message() !!}
    <!-- jquery JS (use the newer version) -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {

        // finished task
        $(document).on("click",'#done', function(e) {
            e.preventDefault();
            let taskId = $(this).data('task-id');
            let currentStatus = $('#status'+taskId).html();
console.log(currentStatus)
console.log(taskId)
            $.ajax({
                url: "{{ route('project.finished') }}",
                method: "post",
                data: {
                    currentStatus: currentStatus,
                    taskId: taskId
                },
                success: function(response) {
                    if(response.status == 'success'){
                        $('#status'+taskId).html('Finished');
                        $('#status'+taskId).removeClass('btn btn-secondary');
                        $('#status'+taskId).addClass('btn btn-success');
                        Command: toastr["success"]("Task Finished")

                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "newestOnTop": true,
                                    "progressBar": true,
                                    "positionClass": "toast-top-right",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                }
                    }
                },
                error: function(err) {
                            console.log(err.responseJSON);

                        }
            });
        });



        // search task
        $(document).on("keyup", function(e) {
            e.preventDefault();

            let search_string = $('#search').val();
            console.log(search_string);
            $.ajax({
                url: "{{ route('search') }}",
                method: "GET",
                data: {
                    search_string: search_string
                },
                success: function(response) {
                    // console.log(response);
                    $('.table-data').html(response);
                    if(response.status == 'nothing_found'){
                        $('.table-data').html('<span class="text-danger">'+'Nothing Found'+'</span>');
                    }
                },
            });
        });

    });


    </script>

</body>

</html>
