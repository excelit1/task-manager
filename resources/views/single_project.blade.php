<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap demo</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    </head>
    <body>
      <button type="submit" class="btn btn-outline-success float-end m-5">Admin</button>
    

    <section class="vh-100" style="background-color: #eee;">
        <div class="container-fluid h-100">
          <div class="row d-flex justify-content-center align-items-center h-75">
            <div class="col col-lg-12 col-xl-9">
              <div class="card rounded-3">
                <div class="card-body p-4">
      
                  <h4 class="text-center my-3 pb-3">Task Details</h4>
      
      
                  <h5>{{$task->title}}</h5>
                  <p></p>
                  <p>Duration : {{$task->duration}}  Hour</p>
                  <p>This task for: {{$task->employee_name}}</p>
                  <p>Description: {{$task->description}}</p>
                  <p>status: {{$task->status}}</p>
                  {{-- <button>status: {{$task}}</button> --}}
                  {{-- @if ($task->status == "Pending")
                  <button class="btn btn-success">Finished</button>
                    
                  @endif --}}

                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>







    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  </body>
</html>