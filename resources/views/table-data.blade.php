<div class="table-data">
    @foreach ($tasks as $key => $task)
            <tr >
                <th scope="row">{{ $key + 1 }}</th>
                <td>
                    <a class="text-decoration-none" href="{{ '/project/' . $task->id }}">
                        {{ $task->title }}
                    </a>
                </td>
                <td>{{ $task->duration }}</td>
                <td>{{ $task->employee_name }}</td>
                <td>
                    <button id="status{{$task->id}}" class="{{$task->status == 'Pending' ? 'btn btn-secondary' : 'btn btn-success'}}">

                        {{ $task->status }}
                    </button>
                </td>
                <td>
                    <button data-task-id="{{$task->id}}" id="done" class="btn btn-success ">Done</button>
                </td>
            </tr>
        @endforeach


</div>
