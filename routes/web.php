<?php

use App\Http\Controllers\backend\EmployeeController;
use App\Http\Controllers\backend\TaskController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;





//frontend routes
Route::get('/', function () {
    return view('welcome');
});

Route::controller(HomeController::class)->group(function () {
    Route::get('/home', 'home');
    Route::get('/', 'home');
    Route::get('/project/{id}', 'details');
    Route::post('/project/finished', 'finished')->name('project.finished');
    Route::get('/search', 'search')->name('search');
});





//backend routes
Route::group(['middleware' => ['auth']], function () {

    Route::get('/dashboard', function () {
        return view('admin.index');
    })->name('dashboard');

    Route::controller(TaskController::class)->group(function () {
        Route::get('/task', 'index')->name('task.index');
        Route::get('/task/create', 'create')->name('task.create');
        Route::get('/task/search', 'search')->name('task.search');
        Route::put('/task/update', 'update')->name('task.update');
        Route::post('/task/store', 'store')->name('task.store');
        Route::post('/task/delete', 'destroy')->name('task.delete');
        Route::post('/task/assain', 'assain')->name('task.assain');
        Route::get('/task/edit/{id}', 'edit');
    });

    Route::controller(EmployeeController::class)->group(function () {

        Route::get('/employees', 'index')->name('employee.index');
        Route::get('/employee/create', 'create')->name('employee.create');
        Route::post('/employee/store', 'store')->name('employee.store');
        Route::get('/employee/search', 'search')->name('employee.search');
        Route::post('/employee/update', 'update')->name('employee.update');
        Route::post('/employee/delete', 'destroy')->name('employee.delete');
        Route::get('/employee/edit/{id}', 'edit');
    });
});








require __DIR__ . '/auth.php';
